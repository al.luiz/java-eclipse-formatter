package foo.bar.baz;

/* example comment */

import java.util.List;
import java.util.Arrays;

import org.eclipse.jdt.core.dom.ASTParser;

public class Example
{
	public interface ExampleProvider
	{
		Example getExample();

		List<Example> getManyExamples();
	}

	public class Pair
	{
		String left;
		String right;
	}

	public Example()
	{
		initialize(1);
	}

	protected void initialize(int value)
	{
		Pair pair = new Pair();
		for (int i = 0; i < value; i++)
		{
			int square = i * i;
			// Between here...

			// ...and here are 10 blank lines
			pair.left = pair.left
					+ square;
		}
		switch (value)
		{
		case 1:
			pair.right = "";
			break;
		default:
			pair.left = "";
		}
	}
}

class Another
{

}
